function stripMatrix(matrix) {
  // strip a two dimentional array
  var rows = matrix.length;
  if (rows == 0) {
    return [];
  }
  var columns = matrix[0].length;

  var row1 = _.find(_.range(rows), function(row) {
    return _.some(matrix[row]);
  });
  if (row1 === undefined) {
    return []
  }
  var col1 = _.find(_.range(columns), function(col) {
    return _.some(_.range(rows), function(row) {
      return matrix[row][col];
    })
  });
  var row2 = _.find(_.range(rows-1, -1, -1), function(row) {
    return _.some(matrix[row]);
  });
  var col2 = _.find(_.range(columns-1, -1, -1), function(col) {
    return _.some(_.range(rows), function(row) {
      return matrix[row][col];
    })
  });
  var ret = [];
  for (var row = row1; row <= row2; row++) {
    var line = [];
    for (var col = col1; col <= col2; col++) {
      line.push(matrix[row][col]);
    }
    ret.push(line);
  }
  return ret;
}


function getColWidths(grid) {
  var cols = grid.countCols();
  var colWidths = _.range(cols).map(grid.getColWidth);
  return colWidths;
}


function checkCell(row, col, value, callback) {
  if (!$.trim(value)) {
    return false;
  }
  var isFeatureCol = col >= 1 && col <= window.featureNum;
  if (isFeatureCol && row == 0) {
    return /(\d+)-(\d+)/.test(value);
  }
  if (value.indexOf('\n') > -1) {
    return false;
  }
  if (!isFeatureCol) {
    return true;
  }
  var featureHeader = grid.getDataAtCell(0, col);
  var m = featureHeader.match(/(\d+)-(\d+)/);
  if (!m) {
    return true;
  }
  var [_, min, max] = m;
  var int_min = parseInt(min, 10);
  var int_max = parseInt(max, 10);
  var int_value = parseInt(value, 10);
  var valid = int_value >= int_min && int_value <= int_max;
  return valid;
}


var container = document.getElementById('grid');

var options = {
  contextMenu: true,
  className: 'htCenter htMiddle',
  rowHeights: 30,
  fixedRowsTop: 1,
  manualColumnResize: true,
  invalidCellClassName: 'invalid-cell',
  colHeaders: true,
  cells: function (row, col, prop) {
    var classNames = ['htCenter', 'htMiddle'];
    if (col >= 1 && col <= window.featureNum) {
      classNames.push('feature-cell');
    }
    return {
      className: classNames.join(' ')
    };
  },
  validator: function(value, callback) {
    var valid = checkCell(this.row, this.col, value);
    callback(valid);
  }
};

var savedColWidths = window.savedColWidths;
if (savedColWidths) {
  options['colWidths'] = savedColWidths;
} else {
  options['colWidths'] = 90;
}


var grid = new Handsontable(container, options);

grid.addHook('afterRender', () => {
  console.log('linkify all');
  $('#grid td').linkify();
});

grid.addHook('afterChange', (changes) => {
  if (changes) {
    grid.changed = true;
  }
  _.each(changes, (change) => {
    var [row, col, oldVal, newVal] = change;
    if (col < 1 || col > window.featureNum) {
      return;
    }
    _.each(_.range(1, grid.countRows()), (_row) => {
      var td = grid.getCell(_row, col);
      var value = grid.getDataAtCell(_row, col);
      var valid = checkCell(_row, col, value);
      if (valid) {
        $(td).removeClass('invalid-cell');
      } else {
        $(td).addClass('invalid-cell');
      }
    });
  });
});


$('#feature-num-selector').change(e => {
  var newFeatureNum = e.target.value;
  window.featureNum = newFeatureNum;
  grid.render();
});

function loadCSV(url, onComplete, onError) {
  Papa.parse(url + '?pure=1', {
    download: true,
    complete: function(results, errors) {
      grid.loadData(results.data);
      onComplete && onComplete();
    },
    error: function(err) {
      onError && onError(err);
    }
  });
}

var url = location.pathname.replace('/csv', '/ajax');
loadCSV(url, null, function(err) {
  if (err == 'NOT FOUND') {
    var url = '/ajax/default_csv_content.txt';
    loadCSV(url, function() {
      $('#feature-num-selector').val(3).trigger('change');
    });
  }
});


$('#save-btn').click(function() {
  var invalid_cells = $('.invalid-cell');
  if (invalid_cells.length) {
    alert('There are invalid cells, plz fix them');
    var elem = invalid_cells[0];
    var {row: row, col: col} = grid.getCoords(elem);
    grid.selectCell(row, col);
    return;
  }
  var data = grid.getData();
  var csv = Papa.unparse(stripMatrix(data), {delimiter: '\t'});
  var data = {
    'content': csv,
    'feature-num': JSON.stringify({
      [location.pathname]: window.featureNum
    }),
    'col-widths': JSON.stringify({
      [location.pathname]: getColWidths(grid)
    })
  };
  var $btn = $(this).button('loading').addClass('btn-danger');
  $.post(url, data)
    .done(function() {
      alert('Saved!')
      grid.changed = false;
    })
    .fail(function() {
      alert('Failed');
      alert('Please manually backup the content in case you lose them all');
    })
    .always(function() {
      $btn.button('reset').removeClass('btn-danger')
    });
});

$('.fork-action').click(function() {
  var lang = $(this).text().trim();
  var url = location.pathname + '/fork';
  $.post(url, {to_lang: lang})
  .done(function(r) {
    if (r.r) {
      alert(r.msg);
    } else {
      var win = window.open(r.url, '_blank');
      win.focus();
    }
  })
  .fail(function() {
    alert('Network Error! Please try again later');
  });
});


function replaceAllCell(grid, func) {
  var data = grid.getData();
  for (var i = 0; i < data.length; i++) {
    for (var j = 0; j < data[i].length; j++) {
      var value = data[i][j];
      data[i][j] = func(value);
    }
  }
  grid.loadData(data);
}

$('#jian-btn').click(function() {
  replaceAllCell(grid, t2p);
});


$('#fan-btn').click(function() {
  replaceAllCell(grid, p2t);
});


window.onbeforeunload = function() {
  if (grid.changed) {
    return 'do you really want to abort all changes?';
  }
};
