window.Utils = {};

Utils.move = function(a, pos1, pos2) {
  // local variables
  var i, tmp;
  // cast input parameters to integers
  pos1 = parseInt(pos1, 10);
  pos2 = parseInt(pos2, 10);
  // if positions are different and inside array
  if (pos1 !== pos2 && 0 <= pos1 && pos1 <= a.length && 0 <= pos2 && pos2 <= a.length) {
    // save element from position 1
    tmp = a[pos1];
    // move element down and shift other elements up
    if (pos1 < pos2) {
      for (i = pos1; i < pos2; i++) {
        a[i] = a[i + 1];
      }
    }
    // move element up and shift other elements down
    else {
      for (i = pos1; i > pos2; i--) {
        a[i] = a[i - 1];
      }
    }
    // put element from position 1 to destination
    a[pos2] = tmp;
  }
};
