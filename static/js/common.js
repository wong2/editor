function openInNewTab(url) {
  var win = window.open(url, '_blank');
  win.focus();
}

function jump(pattern, val) {
  var path = window.location.pathname;
  var new_path = path.replace(new RegExp(pattern, 'g'), val);
  var new_url = location.protocol + '//' + location.host + new_path;
  openInNewTab(new_url);
}

$('#lang_pick').change(function() {
  jump(window.lang, $(this).val());
});

$('#quiz_pick').change(function() {
  jump(window.quiz, $(this).val());
});
