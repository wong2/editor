rivets.binders.append = {

  bind: function(el) {
    var self = this

    this.callback = function() {
      var models = self.view.models;
      self.keypath.split('.').forEach(function(path) {
        models = models[path];
      });
      var data = $(el).data('defaults')
      models.push($.extend(true, {}, data)); // deep copy
    }

    $(el).on('click', this.callback)
  },

  unbind: function(el) {
    $(el).off('click', this.callback)
  }

}


rivets.binders.remove = {

  bind: function(el) {
    var self = this

    this.callback = function() {
      if (!window.confirm('Sure?')) {
        return;
      }
      var models = self.view.models;
      var keypaths = self.keypath.split('.');

      var item_key = keypaths.slice(-1);
      var item = models[item_key];

      var paths = keypaths.slice(0, -1);
      paths.forEach(function(path) {
        models = models[path];
      });

      var index = models.indexOf(item);
      models.splice(index, 1);
    }

    $(el).on('click', this.callback)
  },

  unbind: function(el) {
    $(el).off('click', this.callback)
  }

}
