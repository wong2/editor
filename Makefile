c:
	coffee -w -o static/js -c coffee/* &

build:
	coffee -o static/js -c coffee/*
	uglifyjs libs/jquery/dist/jquery.min.js libs/bootstrap/dist/js/bootstrap.min.js libs/json-editor/dist/jsoneditor.js libs/html.sortable/dist/html.sortable.min.js libs/rivets/dist/rivets.bundled.min.js static/js/rivets.extend.js libs/FileSaver.js/FileSaver.min.js > static/vendor.js
	uglifyjs static/js/parser.js static/js/utils.js static/js/main.js > static/app.js

dev: c build
	python -m SimpleHTTPServer

hook:
	githook -c githook.ini -l 0.0.0.0 -p 9010
