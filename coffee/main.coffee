loading = $('#mask')
pubsub = $({})
changed = false


setHintText = (path, hint_cn) ->
  $label = $('[data-schemapath="' + path + '"]').children('label:eq(0)')
  if $label.find('.hint').length == 0
    $label.append($('<span class="hint" style="margin-left:5px; color:#6a3">').html(hint_cn))


get_lang = ->
  try
    if location.hostname == 'qe.arealme.com'
      return location.pathname.split('/')[2]
    else
      return location.pathname.split('/')[1]
  catch
    return ''

is_lang = (lang) ->
  return get_lang() == lang


QuestionEditor =

  el: $('#question-editor')

  reset: ->
    @el.hide().find('.sortable').sortable('destroy')
    $('#sort-btn').text('Sort').hide().off 'click', @onSortClick
    $('.question-full').show()
    $('.question-fold').hide()
    @file = null

  load: (text) ->
    @questions = QuestionParser.load text

    if @view
      @view.update questions: []
      @view.update questions: @questions
    else
      @view = rivets.bind @el, questions: @questions

      originalValueRoutine = rivets.binders.value.routine

      rivets.binders.value.routine = (el, value) ->
        result = originalValueRoutine(el, value)
        pubsub.trigger 'change'
        pubsub.trigger 'input'
        return result

    @el.show()
    $('#sort-btn').show().on 'click', @onSortClick
    pubsub.trigger 'change'
    pubsub.trigger('loaded')

  onSortClick: ->
    $('.question-full').toggle()
    $('.question-fold').toggle()
    $sortable = $('#question-editor .sortable')
    $btn = $('#sort-btn').toggleClass 'sorting'
    if not $btn.hasClass 'sorting'
      $btn.text 'Sort'
      $sortable.sortable('destroy')
    else
      $btn.text 'Exit Sort'
      $sortable.sortable().bind 'sortupdate', (e, ui) ->
        from_index = ui.oldindex
        to_index = ui.item.index()
        if from_index != to_index
          questions = QuestionEditor.questions
          Utils.move questions, from_index, to_index
          questions.forEach (q, index) ->
            q.index = index + 1

  dump: ->
    text = QuestionParser.dump @questions

  report: (template) ->
    [questionTemplate, answerTemplate] = $.trim(template).split('\n')
    reports = @questions.map (q) ->
      s = questionTemplate.replace('%QUESTION_ID%', q.index)
      s = s.replace('%QUESTION%', q.content)
      lines = [s]
      q.answers.forEach (answer) ->
        if answer.score != '0'
          s = answerTemplate.replace('%ANSWER_TEXT%', answer.content)
          s = s.replace('%ANSWER_VALUE%', answer.score)
          lines.push(s)
      return lines.join('\n')
    return reports.join('\n\n')

  validate: ->
    minScore = maxScore = 0
    hasNumericQuestion = false
    error = null

    @questions.forEach (q, index) ->
      for a in q.answers
        if not /[a-zA-Z0-9\,]+/.test(a.score)
          error = "Question #{index+1} has an invalid answer value"
          return
        if not $.isNumeric(a.score)
          return
      hasNumericQuestion = true
      scores = (parseInt(a.score, 10) for a in q.answers)
      minScore += Math.min.apply(null, scores)
      maxScore += Math.max.apply(null, scores)

    el = $('#validate-result').off().css('color', 'red')
    if error
      el.text('invalid').on 'click', -> alert(error)
    else if not hasNumericQuestion
      el.text('valid')
    else
      el.text("#{minScore} ~ #{maxScore}")

  save: ->
    if $('.text-warning').length
      alert 'text has new lines in it, fix it first!'
      return
    text = @dump()
    try
      QuestionParser.load text
    catch e
      alert 'There are errors, please fix first'
      return
    return text


window.QuestionEditor = QuestionEditor


JSONEditor =

  el: $('#json-editor')

  editor: new window.JSONEditor document.getElementById('json-editor'),
    theme: 'bootstrap3'
    disable_array_add: true
    disable_array_delete: true
    disable_array_reorder: true
    disable_collapse: true
    disable_edit_json: true
    disable_properties: true
    schema: {}

  reset: ->
    @el.hide()
    @file = null

  load: (text) ->
    obj = JSON.parse text
    #obj = @format(obj)
    @editor.setValue(obj)

    parent = $('[data-schemapath="root.content"] .row').parent().eq(0)
    sortings = (window.sortings or []).reverse()
    for sorting in sortings
      $('[data-schemapath="root.content.' + sorting + '"]').parent('.row').prependTo(parent)

    if is_lang('cn')
      setHintText('root.content.title', '这里填入测试标题')
      setHintText('root.content.prestige_prefix', '这里是测试结果的前半句话，中间是具体的结果，比如分数，或者一个物件')
      setHintText('root.content.prestige_suffix', '这里是测试结果的后半句话')
      setHintText('root.content.description', '这里用1~2句总共尽量不超过140汉字的话来描述这个测试，会作为摘要显示在一些地方')
      setHintText('root.content.keywords', '这里是测试的关键词，用半角逗号隔开，比如《智商测试》可以写 <u>智力, 智商测试, 智力测验</u>')
      setHintText('root.content.tip1', '测试封面的第一段文字，建议不超过200字。')
      setHintText('root.content.tip2', '测试封面的第二段文字，建议不超过200字。')
      setHintText('root.content.updates1', '更新信息，内容可以留着不动，但日期最好改为你编辑的日期')
      setHintText('root.content.updates2', '第二条更新信息，内容可以留着不动，但日期最好改为你编辑的日期')
      setHintText('root.content.result_title', '结果提示语言')
      setHintText('root.content.more_description', '这个由兔子填')
      setHintText('root.content.average_result_description', '这个由兔子填')

    if not is_lang('cn') and not is_lang('zh')
      setHintText("root.content.title", "Input the title of this quiz")
      setHintText("root.content.prestige_prefix", "This is the first half of the sentence to share after you're done with the quiz.")
      setHintText("root.content.prestige_suffix", "This is the second half of that sentence. So please make sure you have correctedly put the blanks")
      setHintText("root.content.description", "This is a short breif ot this test.")
      setHintText("root.content.keywords", "This is a group of keywords of this quiz, for instance, if we are working on an IQ quiz, it'd be: <u>IQ, IQ test, IQ quiz, intelligence quotient</u>")
      setHintText("root.content.tip1", "This is the first paragraph on the front page of this quiz")
      setHintText("root.content.tip2", "This is the second paragraph on the front page of this quiz")
      setHintText("root.content.updates1", "This is a update info. You can change the date to today if you see a date below")
      setHintText("root.content.updates2", "This is another update info. You can change the date to today if you see a date below")
      setHintText("root.content.result_title", "This is the hint of the result showing")
      setHintText("root.content.more_description", "")
      setHintText("root.content.average_result_description", "")

    @el.show()

    if obj.content
      $.each obj.content, (key) ->
        path = "root.content.#{key}"
        label = $('[data-schemapath="' + path + '"]').children('label')
        if label.find('.delete-by-key').length == 0
          del_btn = $('<span class="delete-by-key" data-key="' + key + '">x</span>')
          label.prepend(del_btn)
          if window.advanced_mode
            del_btn.show()

    last_row = $("div[data-schematype='object'][data-schemapath='root.content'] .row:last")
    if last_row.siblings('.btn-add-key').length == 0
      add_btn = $('<button type="button" class="btn btn-default btn-add-key">Add New One</button>')
      add_btn.insertAfter(last_row)
      if window.advanced_mode
        add_btn.show()

    if $('#show-advance-btn').length == 0
      btns = '<button id="show-advance-btn" type="button" class="btn btn-default btn-sm">Enter Advanced Mode</button>'
      btns += '&nbsp;<button class="btn btn-default btn-sm" id="aw_advanced_edit_value">Import</button>'
      $('#json-editor').append(btns)

    @editor.on 'change', ->
      pubsub.trigger('change')

    $('input[type=text]').on 'input', ->
      pubsub.trigger('input')

    pubsub.trigger('change')
    pubsub.trigger('loaded')

  dump: ->
    text = JSON.stringify @editor.getValue()

  save: ->
    @dump()

  validate: ->


initFileOpener = ->

  $(window).on 'dragover', (e) ->
    e.preventDefault()

  $(window).on 'drop', (e) ->
    e.preventDefault()

  $('body').on 'drop', (e) ->
    e.preventDefault()
    file = e.originalEvent.dataTransfer.files[0]
    open file

  $('#dropzone').on 'click', ->
    $('#file-handler').click()

  $('#open-btn').on 'click', ->
    $('#file-handler').click()

  $('#file-handler').on 'change', (e) ->
    file = e.originalEvent.target.files[0]
    open file


initRemoteCSS = ->
  #url = 'http://www.arealme.com/editor-default.css?random=' + Math.random()
  #$('head').append "<link rel='stylesheet' type='text/css' href='" + url + "' />"


currentEditor = null

open = (file) ->
  reader = new FileReader()
  reader.onload = (e) ->
    load(file.name, reader.result)
  reader.readAsText file


save = ->
  text = currentEditor.save()
  if not text
    return


  if currentEditor.filename.indexOf('/') > -1
    $btn = $('#save-btn').button('loading').addClass('btn-danger')
    $.post('/ajax' + currentEditor.filename, {content: text})
     .done ->
       alert('Saved!')
       changed = false
     .fail () -> alert('Failed! Please manually backup the content in case you lose them all!')
     .always () -> $btn.button('reset').removeClass('btn-danger')
  else
    blob = new Blob [text], type: 'text/plain;charset=utf-8'
    saveAs blob, currentEditor.filename


load = (path, content) ->
  ext = path.split('.').pop()
  if ext != 'txt' and ext != 'json'
    alert 'Incorrect file type'
    return

  $('#dropzone').hide()
  $('#save-btn')
    .off('click', save)
    .on('click', save)
    .show()

  currentEditor?.reset()

  if ext == 'txt'
    currentEditor = QuestionEditor
  else
    currentEditor = JSONEditor

  window.currentEditor = currentEditor

  currentEditor.filename = path
  loading.show()
  currentEditor.load content

  loading.hide()
  document.title = 'Editor - ' + path

  data = currentEditor.dump()
  showWordCount data


loadRemoteFile = (path) ->
  $.get('/ajax' + path)
    .done (data) ->
      data = JSON.parse(data)
      load(path, data.content)
      $('.mtime').text('last modified: ' + data.mtime).show()
    .fail () -> alert('error')


stripHTML = (text) ->
  tmp = document.createElement('div')
  tmp.innerHTML = text
  tmp.textContent || tmp.innerText || ''


countWord = (text, isEnglish) ->
  text = stripHTML text
  if isEnglish
    text.split(' ').length
  else
    text.length


joinNodes = (o) ->
  text = ''
  for key of o
    v = o[key]
    if typeof(v) == 'object'
      text += joinNodes v
    else
      text += v
  return text


showWordCount = (data) ->
  try
    data = JSON.parse(data)
    text = joinNodes data
    is_txt = false
  catch e
    text = data
    is_txt = true

  count_by_words = countWord text, true
  count_by_chars = countWord text, false
  msg = 'count by characters: ' + count_by_chars + ' &nbsp;&nbsp;|&nbsp;&nbsp;count by words: ' + count_by_words
  if is_txt
    msg += '<br><br>'
    msg += '&nbsp;<button class="btn btn-default btn-sm" id="aw_advanced_edit_value">Import</button>'
    msg += '&nbsp;<button class="btn btn-default btn-sm" id="aw_get_report">Get Report</button>'
  $('.word-count').html(msg)


$ ->
  initFileOpener()
  initRemoteCSS()

  if location.pathname != '/'
    loadRemoteFile(location.pathname)

  $('body').on 'keypress', '.question-content', (e) ->
    if e.keyCode == 13
      e.preventDefault()
      alert 'no enter!'

  $('body').on 'keyup', '.question-content', (e) ->
    if $(this).hasClass('text-warning') and $(this).val().indexOf('\n') == -1
      $(this).removeClass('text-warning')

  $('body').on 'paste', '.question-content', (e) ->
    pastedData = e.originalEvent.clipboardData.getData('text')
    if pastedData and pastedData.indexOf('\n') != -1
      alert 'pasted text has enter in it !'
      $(this).addClass('text-warning')

  pubsub.on 'change', ->
    elem = $('#copy-area')
    if not elem.length
      elem = $('<textarea id="copy-area" class="form-control center-block" rows="10" style="width:70%"></textarea>').appendTo('body')
    data = currentEditor.dump()
    elem.val data

  pubsub.on 'input', ->
    changed = true

  $('body').on 'click', '#aw_advanced_edit_value', ->
    $('#import-modal').modal('show')

  $('body').on 'click', '#aw_get_report', ->
    if !currentEditor.report
      return
    elem = $('#report-template')
    if elem.length
      report = currentEditor.report(elem.val())
      $('#copy-area').val report
    else
      elem = $('<textarea id="report-template" class="form-control center-block" rows="10" style="width:70%"></textarea>')
      elem.insertBefore('#copy-area')
      $.get "/answer_templates/#{get_lang()}", (txt) -> elem.val(txt)

  $('body').on 'click', '#import-btn', ->
    text = $('#import-modal textarea').val()
    if !text
      alert('cant be empty')
      return
    try
      currentEditor.load text
    catch e
      alert('wrong format')
      return

    $('#import-modal').modal('hide')
    $('#import-modal textarea').val('')
    currentEditor.load text


  $('body').on 'click', '.delete-by-key', ->
    key = $(this).data('key')
    obj = currentEditor.editor.getValue()
    delete obj.content[key]
    currentEditor.load(JSON.stringify(obj))

  $('body').on 'click', '.btn-add-key', ->
    obj = currentEditor.editor.getValue()
    key = window.prompt('Input the new key: ')
    if not key
      alert 'Key cannot be empty!'
      return
    if obj.content.hasOwnProperty(key)
      alert 'Key already existed!'
      return
    obj.content[key] = ''
    currentEditor.load(JSON.stringify(obj))

  $('body').on 'click', '#show-advance-btn', ->
    $('.delete-by-key, .btn-add-key').show()
    $(this).hide()
    window.advanced_mode = true

  $('body').on 'change', '.score-input', ->
    currentEditor.validate()

  pubsub.on('loaded', -> currentEditor.validate())

  window.onbeforeunload = ->
    if changed
      return 'do you really want to abort all changes?'
