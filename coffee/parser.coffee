window.QuestionParser = {}

QuestionParser.load = (text) ->
  text.split('|').map (part, index) ->
    lines = part.match(/[^\r\n]+/g).filter Boolean
    return {
      index: index+1
      content: lines[0]
      answers: lines.slice(1).map (line) ->
        m = line.match /\(([a-zA-Z0-9\,]+)\)([^\n]+)/
        return score: m[1], content: m[2]
    }

QuestionParser.dump = (questions) ->
  parts = questions.map (question) ->
    lines = [question.content]
    question.answers.forEach (answer) ->
      lines.push "(#{answer.score})#{answer.content}"
    lines.join '\n'

  text = parts.join '\n|\n'
