#-*-coding:utf-8-*-

import os
import json
import mimetypes
import shutil
import arrow
from flask import (Flask, request, send_file, abort, render_template,
                   redirect, url_for, flash, make_response,
                   send_from_directory, jsonify)
from flask.ext.assets import Environment
from raven.contrib.flask import Sentry
from vedis import Vedis


app = Flask(__name__)
app.config['SECRET_KEY'] = 'haufekefjf'
app.config['DEBUG'] = True
app.config['SENTRY_DSN'] = 'https://c9abbc0336c04a1fb9c6d4930227f34b:15ca60524a2246d0913f73865e11631d@app.getsentry.com/72755'

sentry = Sentry(app)
assets = Environment(app)
db = Vedis('store.db')


ALL_LANGS = 'ar,az,cn,cs,da,de,el,en,es,fa,fi,fr,hi,hu,id,it,ja,ko,ms,nl,no,pl,pt,ro,ru,sv,sw,th,tr,vi,zh'.split(',')


EDITOR_FILES = [
    {
        'label': 'json',
        'path': 'console/project/{quiz}/{lang}/data.json',
        'url': '/{quiz}/{lang}/data.json'
    },
    {
        'label': 'txt',
        'path': 'console/project/{quiz}/{lang}/questions.txt',
        'url': '/{quiz}/{lang}/questions.txt'
    },
    {
        'label': 'feature_data',
        'path': 'files/{lang}/{quiz}/{quiz}_db_src_{lang}.txt',
        'url': '/csv/{lang}/{quiz}/{quiz}_db_src_{lang}.txt'
    },
]

def get_editor_urls(lang, quiz):
    editor_urls = []
    for x in EDITOR_FILES:
        path = x['path'].format(lang=lang, quiz=quiz)
        url = x['url'].format(lang=lang, quiz=quiz)
        if os.path.exists(path):
            editor_urls.append((x['label'], url))
    return editor_urls


def get_langs(lang, quiz, path):
    langs = []

    for editor_file in EDITOR_FILES:
        path_pattern = editor_file['path']
        if path_pattern.format(lang=lang, quiz=quiz) == path:
            break

    for lang_ in os.listdir('console/project'):
        if os.path.exists(path_pattern.format(lang=lang_, quiz=quiz)):
            langs.append(lang_)

    return sorted(langs)


def get_quizs(lang, quiz, path):
    quizs = []
    for quiz_ in os.listdir('console/project'):
        path_ = path.replace(quiz, quiz_)
        if os.path.exists(path_):
            quizs.append(quiz_)
    return sorted(quizs)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/static/js/<path:path>')
def serve_js(path):
    return send_from_directory('static/js', path)


@app.route('/answer_templates/<lang>')
def serve_answer_template(lang):
    path = 'answer_templates/{lang}.txt'.format(lang=lang)
    if not os.path.exists(path):
        path = 'answer_templates/default.txt'
    return send_file(path)


@app.route('/<quiz>/<lang>/<filename>')
def editor(quiz, lang, filename):
    path = 'console/project/{}/{}/{}'.format(quiz, lang, filename)

    if 'download' not in request.args:
        editor_urls = get_editor_urls(lang, quiz)
        langs = get_langs(lang, quiz, path)
        quizs = get_quizs(lang, quiz, path)
        return render_template('index.html', lang=lang, quiz=quiz,
                                             editor_urls=editor_urls,
                                             langs=langs, quizs=quizs)

    if '..' in path or path.startswith('/'):
        abort(404)
    return send_file(path, as_attachment=True)


@app.route('/ajax/<path:path>', methods=['GET', 'POST'])
def ajax(path):
    file_path = 'console/project/' + path
    if request.method == 'GET':
        if not os.path.exists(file_path):
            abort(404)

        with open(file_path) as fp:
            content = fp.read()

        if request.args.get('pure'):
            r = make_response(content)
            mimetype, encoding = mimetypes.guess_type(file_path)
            r.mimetype = mimetype or 'text/plain'
            return r
        else:
            mtime = os.path.getmtime(file_path)
            return json.dumps({
              'content': content,
              'mtime': arrow.get(mtime).to('Asia/Taipei').format('YYYY-MM-DD HH:mm:ss')
            })

    data = dict((key, request.form[key]) for key in request.form.keys())
    content = data.pop('content')
    if not content:
        abort(400)

    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)

    with open(file_path, 'w') as fp:
        try:
            content = json.dumps(json.loads(content), indent=2, ensure_ascii=False)
        except:
            pass
        finally:
            fp.write(content.encode('utf-8'))

    with db.transaction():
        for hashname, json_str in data.items():
            obj = json.loads(json_str)
            for k, v in obj.items():
                db.hset(hashname, k, v)

    return 'ok'


def _get_file_path(file_id, lang):
    return 'files/kyi/{file_id}/{lang}.json'.format(file_id=file_id, lang=lang)

def _read_json(file_path):
    if os.path.exists(file_path):
        with open(file_path) as fp:
            return json.load(fp)

def _write_json(file_path, data):
    if not os.path.exists(os.path.dirname(file_path)):
        os.makedirs(os.path.dirname(file_path))
    with open(file_path, 'w') as fp:
        json.dump(data, fp)

def _file_times(file_path):

    def _format(t):
        return arrow.get(t).to('Asia/Taipei').format('YYYY-MM-DD HH:mm:ss')

    if not os.path.exists(file_path):
        return 'NaN', 'NaN'

    ctime = 'NaN'
    mtime = os.path.getmtime(file_path)
    return ctime, _format(mtime)



@app.route('/knowyourinside/content/<file_id>/', methods=['GET', 'POST'])
def kyi_base(file_id):
    file_path = _get_file_path(file_id, 'en')

    if request.method == 'GET':
        data = _read_json(file_path) or []
    else:
        data = request.form.getlist('text')
        _write_json(file_path, data)

    disabled = 'disabled' if data else ''
    return render_template('kyi/base.html', data=data, disabled=disabled, save_btn='Publish')



@app.route('/knowyourinside/content/<file_id>/<lang>/', methods=['GET', 'POST'])
def kyi_trans(file_id, lang):
    base_file_path = _get_file_path(file_id, 'en')
    file_path = _get_file_path(file_id, lang)

    base_data = _read_json(base_file_path)
    if not base_data:
        flash('You need to create this content first')
        return redirect(url_for('kyi_base', file_id=file_id))

    if request.method == 'GET':
        data = _read_json(file_path) or []
        data += ([''] * (len(base_data) - len(data)))
    else:
        data = request.form.getlist('text')
        _write_json(file_path, data)

    data = zip(base_data, data)
    ctime, mtime = _file_times(file_path)

    word_count = len(' '.join(base_data).split(' '))
    char_count = len(''.join(base_data))

    return render_template('kyi/trans.html',
        data=data, ctime=ctime, mtime=mtime, save_btn='Save',
        word_count=word_count, char_count=char_count
    )


QUIZ_JSON_PATH = 'console/project/{quiz}/{lang}/data.json'

@app.route('/csv/<quiz>/<lang>/<path:filename>')
def csv_editor(quiz, lang, filename):
    path = 'console/project/{}/{}/{}'.format(quiz, lang, filename)
    if 'download' in request.args:
        if '..' in path or path.startswith('/'):
            abort(404)
        return send_file(path, as_attachment=True)

    json_path = QUIZ_JSON_PATH.format(quiz=quiz, lang=lang)
    try:
        with open(json_path) as fp:
            data = json.load(fp)
            quiz_title = data['content']['title']
    except Exception as e:
        return str(e), 500

    key = request.path
    feature_num = int(db.hget('feature-num', key) or 1)
    col_widths = db.hget('col-widths', key) or 90

    editor_urls = get_editor_urls(lang, quiz)
    langs = get_langs(lang, quiz, path)
    quizs = get_quizs(lang, quiz, path)

    if os.path.exists(path):
        mtime = os.path.getmtime(path)
        last_modified = arrow.get(mtime).to('Asia/Taipei').format('YYYY-MM-DD HH:mm:ss')
        with open(path) as fp:
            content = fp.read()
        content = content.replace(',', ' ')
        char_count = len(content)
        word_count = len(content.split(' '))
    else:
        last_modified = 'NaN'
        char_count = word_count = 0

    return render_template('csv/editor.html', quiz_title=quiz_title,
                                              feature_num=feature_num,
                                              langs=langs, quizs=quizs,
                                              editor_urls=editor_urls,
                                              lang=lang, quiz=quiz, ALL_LANGS=ALL_LANGS,
                                              col_widths=col_widths, last_modified=last_modified,
                                              char_count=char_count, word_count=word_count)


@app.route('/csv/<lang>/<quiz>/<path:filename>/fork', methods=['POST'])
def csv_editor_fork(lang, quiz, filename):
    source_path = 'files/{}/{}/{}'.format(lang, quiz, filename)
    dest_lang = request.form['to_lang']
    dest_filename = '{}_db_src_{}.txt'.format(quiz, dest_lang)
    dest_path = 'files/{}/{}/{}'.format(dest_lang, quiz, dest_filename)

    if os.path.exists(dest_path):
        return jsonify(r=1, msg='The csv file exists: %s' % dest_filename)

    dest_json_path = QUIZ_JSON_PATH.format(lang=dest_lang, quiz=quiz)
    if not os.path.exists(dest_json_path):
        return jsonify(r=1, msg='The json file not exists: %s' % dest_json_path)

    try:
        os.makedirs(os.path.dirname(dest_path))
    except Exception as e:
        print e

    shutil.copyfile(source_path, dest_path)
    key = '/csv/{}/{}/{}'.format(lang, quiz, filename)
    feature_num = db.hget('feature-num', key)
    if feature_num:
        fork_key = '/csv/{}/{}/{}'.format(dest_lang, quiz, dest_filename)
        db.hset('feature-num', fork_key, feature_num)

    url = url_for('csv_editor', lang=dest_lang, quiz=quiz, filename=dest_filename)
    return jsonify(r=0, url=url)



if __name__ == '__main__':
    app.run(debug=True, port=8000)
